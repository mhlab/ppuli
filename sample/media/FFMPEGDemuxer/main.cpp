#include "media/demuxer/FFMPEGDemuxer.h"
#include "log/Logger.h"
#include <iostream>

int main(int argc, char *argv[])
{
    PPuli::Media::FFMPEGDemuxer demuxer;
    for (int i = 0; i < 100; i++)
    {
        demuxer.open("/invalid_path/invalid_name.mp4", PPuli::Media::DEMUX_VIDEO);
        demuxer.close();
        std::this_thread::sleep_for(std::chrono::milliseconds(60));
        PPuli::Log::Logger::logI("Current Index: %d", i);
    }

    for (int i = 0; i < 100; i++)
    {
        demuxer.open("/home/leemyunghoon/Video/StarWars.mp4", PPuli::Media::DEMUX_VIDEO);
        demuxer.close();
        std::this_thread::sleep_for(std::chrono::milliseconds(60));
        PPuli::Log::Logger::logI("New Current Index: %d", i);
    }

    std::cin.get();

    return 0;
}