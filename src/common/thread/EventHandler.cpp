#include "EventHandler.h"

using PPuli::Thread::BaseEvent;
using PPuli::Thread::EventHandlerListener;
using PPuli::Thread::EventHandler;

BaseEvent::BaseEvent(EventHandlerListener* listener):
    _listener(listener)
{
}

EventHandlerListener* BaseEvent::eventHandlerListener()
{
    return _listener;
}

int BaseEvent::eventId()
{
    return -1;
}

EventHandler::EventHandler():
    _isTerminated(false)
{
    _thread = new std::thread(&EventHandler::handleEvent, this);
}

EventHandler::~EventHandler()
{
    _isTerminated = true;
    _thread->join();
    delete _thread;
}

void EventHandler::handleEvent()
{
    BaseEvent* event;
    while (!_isTerminated)
    {
        _cond.wait();
        if (_isTerminated == true)
        {
            break;
        }
        event = _queue.pop();
        if (event == NULL)
        {
            continue;
        }
        if (event->eventHandlerListener() != NULL)
            event->eventHandlerListener()->onHandleEvent(event);

        delete event;
        if (_queue.size() > 0)
            _cond.notify();
    }
    event = _queue.pop();
    while (event != NULL)
    {
        delete event;
        event = _queue.pop();
    }
}

void EventHandler::registerEvent(BaseEvent* event)
{
    _queue.push(event);
    _cond.notify();
}