#include <thread>
#include "common/collection/ConcurrentQueue.h"
#include "ConditionalEvent.h"

namespace PPuli::Thread
{
    class EventHandlerListener;

    class BaseEvent
    {
    private:
        EventHandlerListener* _listener;
    
    public:
        BaseEvent(EventHandlerListener* listener);
        virtual ~BaseEvent() {}

    public:
        EventHandlerListener* eventHandlerListener();
        virtual int eventId();
    };

    class EventHandlerListener
    {
    public:
        virtual void onHandleEvent(BaseEvent* event) = 0;
    };

    class EventHandler 
    {
    private:
        std::thread* _thread;
        bool _isTerminated;
        PPuli::Collection::ConcurrentQueue<BaseEvent*> _queue;
        ConditionalEvent _cond;

        void handleEvent();

    public:
        EventHandler();
        ~EventHandler();
    
        void registerEvent(BaseEvent* event);
    };
}