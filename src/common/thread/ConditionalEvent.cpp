#include "ConditionalEvent.h"

using PPuli::Thread::ConditionalEvent;

ConditionalEvent::ConditionalEvent():
    _isNotify(false)
{
}

void ConditionalEvent::wait()
{
    std::unique_lock<std::mutex> lock(_mutex);
    if (_isNotify == false)
        _cond.wait(lock);
    _isNotify = false;
}

void ConditionalEvent::notify()
{
    std::unique_lock<std::mutex> lock(_mutex);
    _cond.notify_all();
    _isNotify = true;
}