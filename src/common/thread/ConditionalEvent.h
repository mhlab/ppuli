#include <mutex>
#include <condition_variable>

namespace PPuli::Thread
{
    class ConditionalEvent
    {
    private:
        std::mutex _mutex;
        std::condition_variable _cond;
        bool _isNotify;

    public:
        ConditionalEvent();

    public:
        void wait();
        void notify();
    };
}