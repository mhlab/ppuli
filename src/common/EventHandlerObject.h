#include "common/thread/EventHandler.h"

namespace PPuli::Common {
    class EventHandlerObject: public Thread::EventHandlerListener {
    private:
        PPuli::Thread::EventHandler* _eventHandler;
        bool _isOwnedEventHandler;

    protected:
        virtual void onHandleEvent(Thread::BaseEvent* event) {};

    public:
        EventHandlerObject(bool createEventHandler);
        virtual ~EventHandlerObject();

    public:
        bool registerEvent(PPuli::Thread::BaseEvent* event);
    };
}