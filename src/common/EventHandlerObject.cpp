#include "EventHandlerObject.h"

using PPuli::Common::EventHandlerObject;
using PPuli::Thread::BaseEvent;

EventHandlerObject::EventHandlerObject(bool createEventHandler):
    _isOwnedEventHandler(false)
{
    if (createEventHandler == true)
    {
        _eventHandler = new Thread::EventHandler();
        _isOwnedEventHandler = true;
    }
}

EventHandlerObject::~EventHandlerObject()
{
    if (_eventHandler != NULL && _isOwnedEventHandler == true)
    {
        delete _eventHandler;
    }
}

bool EventHandlerObject::registerEvent(BaseEvent* event)
{
    if (_eventHandler != NULL)
    {
        _eventHandler->registerEvent(event);
        return true;
    }

    return false;
}