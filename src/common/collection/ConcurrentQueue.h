#include <queue>
#include <mutex>

namespace PPuli::Collection {
    template <typename T>   
    class ConcurrentQueue {
    private:
        std::queue<T> _queue;
        std::mutex _lock;

    public:
        inline void push(T item)
        {
            _lock.lock();
            _queue.push(item);
            _lock.unlock();
        }
        inline T pop()
        {
            _lock.lock();
            if (_queue.size() == 0)
            {
                _lock.unlock();
                return NULL;
            }

            T item = _queue.front();
            _queue.pop();
            _lock.unlock();

            return item;
        }
        inline size_t size()
        {
            int size;
            _lock.lock();
            size = _queue.size();
            _lock.unlock();

            return size;
        }
    };
}