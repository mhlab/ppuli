namespace Radix::Common
{
    const int RET_OK = 0;
    enum Status
    {
        UNKNOWN = -1,
        OK = 0,
        FAIL = 1
    };
}