#include "FFMPEGDemuxer.h"
#include "log/Logger.h"
#include <string>

using PPuli::Media::FFMPEGDemuxer;
using PPuli::Thread::BaseEvent;
using PPuli::Thread::EventHandlerListener;
using PPuli::Media::FFMPEGDemuxerMediaType;
using PPuli::Log::Logger;

enum FFMPEGDemuxerEventId
{
    FFMPEGDemuxerEvent_Open,
    FFMPEGDemuxerEvent_Close
};

class OpenEvent: public BaseEvent
{
public:
    std::string _mediaFilePath;
    FFMPEGDemuxerMediaType _mediaType;

public:
    OpenEvent(EventHandlerListener* listener): BaseEvent(listener) {}
    virtual ~OpenEvent() {}
    virtual int eventId() { return (int)FFMPEGDemuxerEvent_Open; }
};

class CloseEvent: public BaseEvent
{
public:
    CloseEvent(EventHandlerListener* listener): BaseEvent(listener) {}
    virtual int eventId() { return (int)FFMPEGDemuxerEvent_Close; }
};

FFMPEGDemuxer::FFMPEGDemuxer(): EventHandlerObject(true),
    _streamIndex(-1),
    _bsfContext(NULL),
    _formatContext(NULL)    
{
    av_register_all();
}

FFMPEGDemuxer::~FFMPEGDemuxer()
{
    internalClose();
}

void FFMPEGDemuxer::onHandleEvent(BaseEvent* event)
{
    switch (event->eventId())
    {
        case FFMPEGDemuxerEvent_Open:
        {
            internalOpen(event);
            break;
        }
        case FFMPEGDemuxerEvent_Close:
        {
            internalClose();
            break;
        }
    }
}

bool FFMPEGDemuxer::open(const char* mediaFile, FFMPEGDemuxerMediaType mediaType)
{
    OpenEvent* event = new OpenEvent(this);
    if (event == NULL)
        return false;

    event->_mediaFilePath = mediaFile;
    event->_mediaType = mediaType;

    registerEvent(event);
    return true;
}

bool FFMPEGDemuxer::internalOpen(BaseEvent* event)
{
    bool ret = false;
    Logger::logI("%s >>", __func__);
    OpenEvent* openEvent = (OpenEvent*)event;
    try
    {
		if (avformat_open_input(&_formatContext, openEvent->_mediaFilePath.c_str(), NULL, NULL) < 0)
			throw 0;
		if (avformat_find_stream_info(_formatContext, NULL) < 0)
			throw 1;
        _streamIndex = -1;
        if (openEvent->_mediaType == DEMUX_VIDEO)
            _streamIndex = av_find_best_stream(_formatContext, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
        else
            _streamIndex = av_find_best_stream(_formatContext, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);
        if (_streamIndex < 0)
            throw 2;
        AVBitStreamFilter* bsf = NULL;
        switch (_formatContext->streams[_streamIndex]->codecpar->codec_id)
        {
            case AV_CODEC_ID_H264:
            {
                bsf = (AVBitStreamFilter*)av_bsf_get_by_name("h264_mp4toannexb");
                break;
            }
            case AV_CODEC_ID_AAC:
            {
                bsf = (AVBitStreamFilter*)av_bsf_get_by_name("aac_adtstoasc");
                break;
            }
            default:
            {
                break;
            }
        }
        if (bsf != NULL)
        {
            int bsfret = av_bsf_alloc(bsf, &_bsfContext);
            if (bsfret == 0) 
            {
                avcodec_parameters_copy(_bsfContext->par_in, _formatContext->streams[_streamIndex]->codecpar);
                av_bsf_init(_bsfContext);
            }
        }
        Logger::logI("Success to open demuxer. FileName: %s", openEvent->_mediaFilePath.c_str());
        ret = true;
    }
    catch(int err)
    {
        Logger::logI("Fail to open demuxer. Err: %d FileName: %s", err, openEvent->_mediaFilePath.c_str());
        Logger::logI("%s <<", __func__);
    }
    catch(...)
    {
        Logger::logI("%s <<", __func__);
    }
    
    Logger::logI("%s <<", __func__);
    return ret;
}

bool FFMPEGDemuxer::close()
{
    CloseEvent* event = new CloseEvent(this);
    if (event == NULL)
        return false;

    registerEvent(event);
    return true;
}

bool FFMPEGDemuxer::internalClose()
{
    Logger::logI("%s >>", __func__);
    if (_formatContext != NULL) {
        avformat_close_input(&_formatContext);
        _formatContext = NULL;
    }
	if (_bsfContext != NULL) {
		av_bsf_free(&_bsfContext);
		_bsfContext = NULL;
	}
    _streamIndex = -1;
    Logger::logI("%s <<", __func__);

    return true;
}