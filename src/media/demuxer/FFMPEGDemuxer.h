#include "common/EventHandlerObject.h"

extern "C" {
#include "libavformat/avformat.h"
}

namespace PPuli::Media {
    enum FFMPEGDemuxerMediaType {
        DEMUX_VIDEO =0,
        DEMUX_AUDIO
    };
    class FFMPEGDemuxer: public Common::EventHandlerObject {
    private:
        int _streamIndex;
        AVBSFContext* _bsfContext;
        AVFormatContext* _formatContext;
        bool internalOpen(Thread::BaseEvent* event);
        bool internalClose();
        
    protected:
        virtual void onHandleEvent(Thread::BaseEvent* event);
    
    public:
        FFMPEGDemuxer();
        virtual ~FFMPEGDemuxer();

    public:
        bool open(const char* mediaFile, FFMPEGDemuxerMediaType mediaType);
        bool close();
    };
}