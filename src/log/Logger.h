namespace PPuli::Log
{
    enum LogType
    {
        WARNING = 0,
        INFO = 1,
        DEBUG = 2,
        ERROR = 3,
        ALL = 0xFF
    };
    class Logger
    {
    private:
        static int _logFilter;

    public:
        static void setLogFilter(int logFilter);
        static void logI(char* log, ...);
    };
}