#include "Logger.h"
#include <cstdio>
#include <stdarg.h>

using PPuli::Log::Logger;

int Logger::_logFilter = LogType::ALL;

void Logger::setLogFilter(int logFilter)
{
    _logFilter = logFilter;
}

void Logger::logI(char* log, ...)
{
    if ((_logFilter & LogType::INFO) > 0)
    {
        char logBuf[2048] = {};
        va_list args;

        va_start(args, log);
        vsprintf(logBuf, log, args);
        printf("[Info]%s\n", logBuf);
        va_end(args);
    }
}